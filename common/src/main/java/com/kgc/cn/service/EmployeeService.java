package com.kgc.cn.service;

import com.kgc.cn.model.Employee;
import com.kgc.cn.vo.*;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface EmployeeService {
    /**
     * 员工登录
     *
     * @param employeeLoginVo
     * @return
     */
    Employee employeeLogin(EmployeeLoginVo employeeLoginVo) throws Exception;

    /**
     * 通过excel批量增加员工信息
     *
     * @param list
     * @return
     */
    boolean addEmpFromExcel(List<EmployeeExcelVo> list);

    /**
     * excel输出一个月内某天的出勤情况
     *
     * @param date
     * @return
     * @throws ParseException
     */
    List<AttendanceExcelVo> showAttendToExcel(String date) throws ParseException;


    /**
     * 冻结用户状态
     *
     * @param id
     * @return
     */
    int updateEmployeeState(String id, Employee employee);


    int recoverEmployeeState(String id, Employee employee);

    /**
     * 删除员工账号
     *
     * @param id
     * @return
     */
    int delEmployee(String id, Employee employee);

    /**
     * 根据任意字段查询员工信息
     *
     * @param employeeQueryVo
     * @return
     */
    Map<Page, List<EmployeeQueryVo>> showEmployee(EmployeeQueryVo employeeQueryVo,
                                                  int pageSize, int startRows, Employee employee);

    /**
     * 修改员工信息
     *
     * @param employeeUpdateVo
     * @param employee
     * @return
     */
    int updateEmployee(EmployeeUpdateVo employeeUpdateVo, Employee employee) throws Exception;

    /**
     * 导出员工表为Excel
     *
     * @return
     */
    List<EmployeeExcelVo> showEmployeeExcelVo(int level);

    /**
     * 员工修改密码
     *
     * @param id
     * @param password
     * @return
     */
    int updatePassword(String id, String password) throws Exception;

    /**
     * 根据id查询
     *
     * @param eid
     * @return
     */
    boolean queryEmployeeById(String eid);

    /**
     * 请假开始
     *
     * @param eid
     */
    void vacationEmployee(String eid);

    /**
     * 请假结束
     *
     * @param eid
     */
    void vacationExpire(String eid);
}
