package com.kgc.cn.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "商品类型数量model")
public class GoodsTypeNumberVo implements Serializable {
    @ApiModelProperty(value = "类型")
    private String property;
    @ApiModelProperty(value = "数量")
    private Integer count;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
