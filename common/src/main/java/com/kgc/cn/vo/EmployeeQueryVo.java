package com.kgc.cn.vo;

import com.kgc.cn.model.Employee;
import com.kgc.cn.utils.conver.ConvertUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "员工查询用model")
public class EmployeeQueryVo implements Serializable {
    @ApiModelProperty(value = "工号", required = true)
    private String eId;
    @ApiModelProperty(value = "姓名")
    private String eName;
    @ApiModelProperty(value = "性别 1：男 2：女")
    private Integer eSex;
    @ApiModelProperty(value = "权限等级，从上到下共5级")
    private Integer eLevel;
    @ApiModelProperty(value = "手机号")
    private String ePhone;
    @ApiModelProperty(value = "邮箱")
    private String eEmail;
    @ApiModelProperty(value = "年龄", example = "0")
    private Integer eAge;
    @ApiModelProperty(value = "账号状态 1正常，2冻结")
    private Integer eStatus;
    @ApiModelProperty(value = "入职时间")
    private Date eEntryTime;
    @ApiModelProperty(value = "所属部门")
    private String eDepartment;
    @ApiModelProperty(value = "薪资")
    private Integer eSalary;
    @ApiModelProperty(value = "当天出勤情况 0：缺席，1：正常出勤，2：迟到，3：请假")
    private Integer eAttendance;


    public String geteId() {
        return eId;
    }

    public void seteId(String eId) {
        this.eId = eId;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public Integer geteSex() {
        return eSex;
    }

    public void seteSex(Integer eSex) {
        this.eSex = eSex;
    }

    public Integer geteLevel() {
        return eLevel;
    }

    public void seteLevel(Integer eLevel) {
        this.eLevel = eLevel;
    }

    public String getePhone() {
        return ePhone;
    }

    public void setePhone(String ePhone) {
        this.ePhone = ePhone;
    }

    public String geteEmail() {
        return eEmail;
    }

    public void seteEmail(String eEmail) {
        this.eEmail = eEmail;
    }

    public Integer geteAge() {
        return eAge;
    }

    public void seteAge(Integer eAge) {
        this.eAge = eAge;
    }

    public Integer geteStatus() {
        return eStatus;
    }

    public void seteStatus(Integer eStatus) {
        this.eStatus = eStatus;
    }

    public Date geteEntryTime() {
        return eEntryTime;
    }

    public void seteEntryTime(Date eEntryTime) {
        this.eEntryTime = eEntryTime;
    }

    public String geteDepartment() {
        return eDepartment;
    }

    public void seteDepartment(String eDepartment) {
        this.eDepartment = eDepartment;
    }

    public Integer geteSalary() {
        return eSalary;
    }

    public void seteSalary(Integer eSalary) {
        this.eSalary = eSalary;
    }

    public Integer geteAttendance() {
        return eAttendance;
    }

    public void seteAttendance(Integer eAttendance) {
        this.eAttendance = eAttendance;
    }

    public Employee toEmployee() {
        ConvertUtil<EmployeeQueryVo, Employee> convertUtil = new ConvertUtil<>(Employee.class);
        return convertUtil.Convert(this);
    }
}
