package com.kgc.cn.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.kgc.cn.model.Discount;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "折扣model")
public class DiscountVo extends BaseRowModel implements Serializable {
    @ExcelProperty(value = "商品id", index = 0)
    @ApiModelProperty(value = "商品id")
    private String id;

    @ExcelProperty(value = "打折开始时间", index = 1)
    @ApiModelProperty(value = "打折开始时间")
    private Date starttime;

    @ExcelProperty(value = "打折结束时间", index = 2)
    @ApiModelProperty(value = "打折结束时间")
    private Date endtime;

    @ExcelProperty(value = "折扣", index = 3)
    @ApiModelProperty(value = "折扣(eg:80%)")
    private Integer count;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }


    public Discount toDiscount() {
        Discount discount = new Discount();
        discount.setdCount(count);
        discount.setdEndTime(endtime);
        discount.setdStartTime(starttime);
        return discount;
    }


}
