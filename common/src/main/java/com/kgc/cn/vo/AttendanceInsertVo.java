package com.kgc.cn.vo;

import com.kgc.cn.model.Attendance;
import com.kgc.cn.utils.conver.ConvertUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;


@ApiModel(value = "出勤情况录入用model")
public class AttendanceInsertVo implements Serializable {

    @ApiModelProperty(value = "员工工号")
    private String eId;

    @ApiModelProperty(value = "员工姓名")
    private String eName;

    @ApiModelProperty(value = "日期")
    private Date aDate;

    @ApiModelProperty(value = "当天出勤情况 0：缺席，1：正常出勤，2：迟到，3：请假")
    private String aSituation;

    public Attendance toAttendance() {
        ConvertUtil<AttendanceInsertVo, Attendance> convertUtil = new ConvertUtil(Attendance.class);
        return convertUtil.Convert(this);
    }

    public String geteId() {
        return eId;
    }

    public void seteId(String eId) {
        this.eId = eId;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public Date getaDate() {
        return aDate;
    }

    public void setaDate(Date aDate) {
        this.aDate = aDate;
    }

    public String getaSituation() {
        return aSituation;
    }

    public void setaSituation(String aSituation) {
        this.aSituation = aSituation;
    }
}
