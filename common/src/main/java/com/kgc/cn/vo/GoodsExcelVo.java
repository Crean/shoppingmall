package com.kgc.cn.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.kgc.cn.model.Goods;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "商品表Model")
public class GoodsExcelVo extends BaseRowModel implements Serializable {

    private static final long serialVersionUID = -6130285497144389538L;
    @ExcelProperty(value = "商品名", index = 0)
    @ApiModelProperty(value = "商品名", required = true)
    private String name;

    @ExcelProperty(value = "价格/分", index = 1)
    @ApiModelProperty(value = "价格/分", required = true)
    private String price;

    @ExcelProperty(value = "类型", index = 2)
    @ApiModelProperty(value = "类型", required = true)
    private String property;

    @ExcelProperty(value = "库存", index = 3)
    @ApiModelProperty(value = "库存", required = true)
    private String stock;

    @ExcelProperty(value = "封面", index = 4)
    @ApiModelProperty(value = "封面", required = true)
    private String cover;

    @ExcelProperty(value = "简介", index = 5)
    @ApiModelProperty(value = "简介", required = true)
    private String content;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Goods toGoods() {
        Goods goods = new Goods();
        goods.setgName(name);
        goods.setgPrice(Long.parseLong(price));
        goods.setgProperty(property);
        goods.setgStock(Integer.parseInt(stock));
        goods.setgCover(cover);
        goods.setgContent(content);
        return goods;
    }
}
