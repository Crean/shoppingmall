package com.kgc.cn.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "出勤记录表Model")
public class AttendanceExcelVo extends BaseRowModel implements Serializable {

    @ExcelProperty(value = "id", index = 0)
    @ApiModelProperty(value = "id")
    private Long id;

    @ExcelProperty(value = "员工工号", index = 1)
    @ApiModelProperty(value = "员工工号")
    private String eid;

    @ExcelProperty(value = "员工姓名", index = 2)
    @ApiModelProperty(value = "员工姓名")
    private String name;

    @ExcelProperty(value = "日期", index = 3)
    @ApiModelProperty(value = "日期")
    private Date date;

    @ExcelProperty(value = "出勤情况", index = 4)
    @ApiModelProperty(value = "出勤情况")
    private String situation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }
}
