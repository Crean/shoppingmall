package com.kgc.cn.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.kgc.cn.model.Employee;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "员工表Model")
public class EmployeeExcelVo extends BaseRowModel implements Serializable {

    @ExcelProperty(value = "姓名", index = 0)
    @ApiModelProperty(value = "姓名", required = true)
    private String name;

    @ExcelProperty(value = "密码", index = 3)
    @ApiModelProperty(value = "密码", required = true)
    private String password;

    @ExcelProperty(value = "性别", index = 4)
    @ApiModelProperty(value = "性别 1：男 2：女", required = true)
    private String sex;

    @ExcelProperty(value = "手机号", index = 1)
    @ApiModelProperty(value = "手机号", required = true)
    private String phone;

    @ExcelProperty(value = "邮箱", index = 2)
    @ApiModelProperty(value = "邮箱", required = true)
    private String email;

    @ExcelProperty(value = "年龄", index = 5)
    @ApiModelProperty(value = "年龄", required = true)
    private String age;

    @ExcelProperty(value = "入职时间", index = 6, format = "yyyy/MM/dd")
    @ApiModelProperty(value = "入职时间", required = true)
    private Date entrytime;

    @ExcelProperty(value = "所属部门", index = 7)
    @ApiModelProperty(value = "所属部门", required = true)
    private String department;

    @ExcelProperty(value = "薪资", index = 8)
    @ApiModelProperty(value = "薪资", required = true)
    private String salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Date getEntrytime() {
        return entrytime;
    }

    public void setEntrytime(Date entrytime) {
        this.entrytime = entrytime;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public Employee toEmployee() {
        Employee employee = new Employee();
        employee.seteName(name);
        employee.setePhone(phone);
        employee.seteEmail(email);
        employee.setePassword(password);
        employee.seteSex(Integer.parseInt(sex));
        employee.seteAge(Integer.parseInt(age));
        employee.seteEntryTime(entrytime);
        employee.seteDepartment(department);
        employee.seteSalary(Integer.parseInt(salary));
        return employee;
    }
}
