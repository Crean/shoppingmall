package com.kgc.cn.vo;

import com.kgc.cn.model.Employee;
import com.kgc.cn.utils.conver.ConvertUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "员工信息修改用model")
public class EmployeeUpdateVo implements Serializable {
    @ApiModelProperty(value = "工号", required = true)
    private String eId;
    @ApiModelProperty(value = "姓名")
    private String eName;
    @ApiModelProperty(value = "密码")
    private String ePassword;
    @ApiModelProperty(value = "权限等级，从上到下共5级")
    private Integer eLevel;
    @ApiModelProperty(value = "手机号")
    private String ePhone;
    @ApiModelProperty(value = "邮箱")
    private String eEmail;
    @ApiModelProperty(value = "所属部门")
    private String eDepartment;
    @ApiModelProperty(value = "薪资", example = "0")
    private Integer eSalary;
    @ApiModelProperty(value = "当天出勤情况 0：缺席，1：正常出勤，2：迟到，3：请假")
    private Integer eAttendance;

    public String geteId() {
        return eId;
    }

    public void seteId(String eId) {
        this.eId = eId;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public String getePassword() {
        return ePassword;
    }

    public void setePassword(String ePassword) {
        this.ePassword = ePassword;
    }

    public Integer geteLevel() {
        return eLevel;
    }

    public void seteLevel(Integer eLevel) {
        this.eLevel = eLevel;
    }

    public String getePhone() {
        return ePhone;
    }

    public void setePhone(String ePhone) {
        this.ePhone = ePhone;
    }

    public String geteEmail() {
        return eEmail;
    }

    public void seteEmail(String eEmail) {
        this.eEmail = eEmail;
    }

    public String geteDepartment() {
        return eDepartment;
    }

    public void seteDepartment(String eDepartment) {
        this.eDepartment = eDepartment;
    }

    public Integer geteSalary() {
        return eSalary;
    }

    public void seteSalary(Integer eSalary) {
        this.eSalary = eSalary;
    }

    public Integer geteAttendance() {
        return eAttendance;
    }

    public void seteAttendance(Integer eAttendance) {
        this.eAttendance = eAttendance;
    }

    public Employee toEmployee() {
        ConvertUtil<EmployeeUpdateVo, Employee> convertUtil = new ConvertUtil<>(Employee.class);
        return convertUtil.Convert(this);
    }
}
