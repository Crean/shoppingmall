package com.kgc.cn.vo;

import com.kgc.cn.model.Goods;
import com.kgc.cn.utils.conver.ConvertUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "商品更新用model")
public class GoodsUpdateVo implements Serializable {
    @ApiModelProperty(value = "商品id")
    private String gId;
    @ApiModelProperty(value = "商品名")
    private String gName;
    @ApiModelProperty(value = "价格/分")
    private Long gPrice;
    @ApiModelProperty(value = "库存")
    private Integer gStock;
    @ApiModelProperty(value = "封面")
    private String gCover;
    @ApiModelProperty(value = "简介")
    private String gContent;
    @ApiModelProperty(value = "类型")
    private String gProperty;

    public String getgId() {
        return gId;
    }

    public void setgId(String gId) {
        this.gId = gId;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public Long getgPrice() {
        return gPrice;
    }

    public void setgPrice(Long gPrice) {
        this.gPrice = gPrice;
    }

    public Integer getgStock() {
        return gStock;
    }

    public void setgStock(Integer gStock) {
        this.gStock = gStock;
    }

    public String getgCover() {
        return gCover;
    }

    public void setgCover(String gCover) {
        this.gCover = gCover;
    }

    public String getgContent() {
        return gContent;
    }

    public void setgContent(String gContent) {
        this.gContent = gContent;
    }

    public String getgProperty() {
        return gProperty;
    }

    public void setgProperty(String gProperty) {
        this.gProperty = gProperty;
    }

    public Goods toGoods() {
        ConvertUtil<GoodsUpdateVo, Goods> convertUtil = new ConvertUtil<>(Goods.class);
        return convertUtil.Convert(this);
    }
}
