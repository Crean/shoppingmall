package com.kgc.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "折扣查询用model")
public class DiscountQueryVo implements Serializable {
    @ApiModelProperty(value = "打折开始时间")
    private Date dStartTime;
    @ApiModelProperty(value = "打折结束时间")
    private Date dEndTime;
    @ApiModelProperty(value = "折扣力度")
    private Integer dCount;
    @ApiModelProperty(value = "是否折扣")
    private Integer dStatus;

    public Date getdStartTime() {
        return dStartTime;
    }

    public void setdStartTime(Date dStartTime) {
        this.dStartTime = dStartTime;
    }

    public Date getdEndTime() {
        return dEndTime;
    }

    public void setdEndTime(Date dEndTime) {
        this.dEndTime = dEndTime;
    }

    public Integer getdCount() {
        return dCount;
    }

    public void setdCount(Integer dCount) {
        this.dCount = dCount;
    }

    public Integer getdStatus() {
        return dStatus;
    }

    public void setdStatus(Integer dStatus) {
        this.dStatus = dStatus;
    }
}
