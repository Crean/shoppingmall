package com.kgc.cn.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DiscountExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table discount
     *
     * @mbg.generated
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table discount
     *
     * @mbg.generated
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table discount
     *
     * @mbg.generated
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbg.generated
     */
    public DiscountExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbg.generated
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbg.generated
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbg.generated
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbg.generated
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbg.generated
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbg.generated
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbg.generated
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbg.generated
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbg.generated
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table discount
     *
     * @mbg.generated
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table discount
     *
     * @mbg.generated
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andDIdIsNull() {
            addCriterion("d_id is null");
            return (Criteria) this;
        }

        public Criteria andDIdIsNotNull() {
            addCriterion("d_id is not null");
            return (Criteria) this;
        }

        public Criteria andDIdEqualTo(Long value) {
            addCriterion("d_id =", value, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdNotEqualTo(Long value) {
            addCriterion("d_id <>", value, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdGreaterThan(Long value) {
            addCriterion("d_id >", value, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdGreaterThanOrEqualTo(Long value) {
            addCriterion("d_id >=", value, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdLessThan(Long value) {
            addCriterion("d_id <", value, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdLessThanOrEqualTo(Long value) {
            addCriterion("d_id <=", value, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdIn(List<Long> values) {
            addCriterion("d_id in", values, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdNotIn(List<Long> values) {
            addCriterion("d_id not in", values, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdBetween(Long value1, Long value2) {
            addCriterion("d_id between", value1, value2, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdNotBetween(Long value1, Long value2) {
            addCriterion("d_id not between", value1, value2, "dId");
            return (Criteria) this;
        }

        public Criteria andDStartTimeIsNull() {
            addCriterion("d_start_time is null");
            return (Criteria) this;
        }

        public Criteria andDStartTimeIsNotNull() {
            addCriterion("d_start_time is not null");
            return (Criteria) this;
        }

        public Criteria andDStartTimeEqualTo(Date value) {
            addCriterion("d_start_time =", value, "dStartTime");
            return (Criteria) this;
        }

        public Criteria andDStartTimeNotEqualTo(Date value) {
            addCriterion("d_start_time <>", value, "dStartTime");
            return (Criteria) this;
        }

        public Criteria andDStartTimeGreaterThan(Date value) {
            addCriterion("d_start_time >", value, "dStartTime");
            return (Criteria) this;
        }

        public Criteria andDStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("d_start_time >=", value, "dStartTime");
            return (Criteria) this;
        }

        public Criteria andDStartTimeLessThan(Date value) {
            addCriterion("d_start_time <", value, "dStartTime");
            return (Criteria) this;
        }

        public Criteria andDStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("d_start_time <=", value, "dStartTime");
            return (Criteria) this;
        }

        public Criteria andDStartTimeIn(List<Date> values) {
            addCriterion("d_start_time in", values, "dStartTime");
            return (Criteria) this;
        }

        public Criteria andDStartTimeNotIn(List<Date> values) {
            addCriterion("d_start_time not in", values, "dStartTime");
            return (Criteria) this;
        }

        public Criteria andDStartTimeBetween(Date value1, Date value2) {
            addCriterion("d_start_time between", value1, value2, "dStartTime");
            return (Criteria) this;
        }

        public Criteria andDStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("d_start_time not between", value1, value2, "dStartTime");
            return (Criteria) this;
        }

        public Criteria andDEndTimeIsNull() {
            addCriterion("d_end_time is null");
            return (Criteria) this;
        }

        public Criteria andDEndTimeIsNotNull() {
            addCriterion("d_end_time is not null");
            return (Criteria) this;
        }

        public Criteria andDEndTimeEqualTo(Date value) {
            addCriterion("d_end_time =", value, "dEndTime");
            return (Criteria) this;
        }

        public Criteria andDEndTimeNotEqualTo(Date value) {
            addCriterion("d_end_time <>", value, "dEndTime");
            return (Criteria) this;
        }

        public Criteria andDEndTimeGreaterThan(Date value) {
            addCriterion("d_end_time >", value, "dEndTime");
            return (Criteria) this;
        }

        public Criteria andDEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("d_end_time >=", value, "dEndTime");
            return (Criteria) this;
        }

        public Criteria andDEndTimeLessThan(Date value) {
            addCriterion("d_end_time <", value, "dEndTime");
            return (Criteria) this;
        }

        public Criteria andDEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("d_end_time <=", value, "dEndTime");
            return (Criteria) this;
        }

        public Criteria andDEndTimeIn(List<Date> values) {
            addCriterion("d_end_time in", values, "dEndTime");
            return (Criteria) this;
        }

        public Criteria andDEndTimeNotIn(List<Date> values) {
            addCriterion("d_end_time not in", values, "dEndTime");
            return (Criteria) this;
        }

        public Criteria andDEndTimeBetween(Date value1, Date value2) {
            addCriterion("d_end_time between", value1, value2, "dEndTime");
            return (Criteria) this;
        }

        public Criteria andDEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("d_end_time not between", value1, value2, "dEndTime");
            return (Criteria) this;
        }

        public Criteria andDCountIsNull() {
            addCriterion("d_count is null");
            return (Criteria) this;
        }

        public Criteria andDCountIsNotNull() {
            addCriterion("d_count is not null");
            return (Criteria) this;
        }

        public Criteria andDCountEqualTo(Integer value) {
            addCriterion("d_count =", value, "dCount");
            return (Criteria) this;
        }

        public Criteria andDCountNotEqualTo(Integer value) {
            addCriterion("d_count <>", value, "dCount");
            return (Criteria) this;
        }

        public Criteria andDCountGreaterThan(Integer value) {
            addCriterion("d_count >", value, "dCount");
            return (Criteria) this;
        }

        public Criteria andDCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("d_count >=", value, "dCount");
            return (Criteria) this;
        }

        public Criteria andDCountLessThan(Integer value) {
            addCriterion("d_count <", value, "dCount");
            return (Criteria) this;
        }

        public Criteria andDCountLessThanOrEqualTo(Integer value) {
            addCriterion("d_count <=", value, "dCount");
            return (Criteria) this;
        }

        public Criteria andDCountIn(List<Integer> values) {
            addCriterion("d_count in", values, "dCount");
            return (Criteria) this;
        }

        public Criteria andDCountNotIn(List<Integer> values) {
            addCriterion("d_count not in", values, "dCount");
            return (Criteria) this;
        }

        public Criteria andDCountBetween(Integer value1, Integer value2) {
            addCriterion("d_count between", value1, value2, "dCount");
            return (Criteria) this;
        }

        public Criteria andDCountNotBetween(Integer value1, Integer value2) {
            addCriterion("d_count not between", value1, value2, "dCount");
            return (Criteria) this;
        }

        public Criteria andDStatusIsNull() {
            addCriterion("d_status is null");
            return (Criteria) this;
        }

        public Criteria andDStatusIsNotNull() {
            addCriterion("d_status is not null");
            return (Criteria) this;
        }

        public Criteria andDStatusEqualTo(Integer value) {
            addCriterion("d_status =", value, "dStatus");
            return (Criteria) this;
        }

        public Criteria andDStatusNotEqualTo(Integer value) {
            addCriterion("d_status <>", value, "dStatus");
            return (Criteria) this;
        }

        public Criteria andDStatusGreaterThan(Integer value) {
            addCriterion("d_status >", value, "dStatus");
            return (Criteria) this;
        }

        public Criteria andDStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("d_status >=", value, "dStatus");
            return (Criteria) this;
        }

        public Criteria andDStatusLessThan(Integer value) {
            addCriterion("d_status <", value, "dStatus");
            return (Criteria) this;
        }

        public Criteria andDStatusLessThanOrEqualTo(Integer value) {
            addCriterion("d_status <=", value, "dStatus");
            return (Criteria) this;
        }

        public Criteria andDStatusIn(List<Integer> values) {
            addCriterion("d_status in", values, "dStatus");
            return (Criteria) this;
        }

        public Criteria andDStatusNotIn(List<Integer> values) {
            addCriterion("d_status not in", values, "dStatus");
            return (Criteria) this;
        }

        public Criteria andDStatusBetween(Integer value1, Integer value2) {
            addCriterion("d_status between", value1, value2, "dStatus");
            return (Criteria) this;
        }

        public Criteria andDStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("d_status not between", value1, value2, "dStatus");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table discount
     *
     * @mbg.generated do_not_delete_during_merge
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table discount
     *
     * @mbg.generated
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}