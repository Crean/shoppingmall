package com.kgc.cn.exception;

public class GoodsDiscountException extends RuntimeException {


    public GoodsDiscountException(String message) {
        super(message);
    }
}
