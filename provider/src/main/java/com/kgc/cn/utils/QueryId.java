package com.kgc.cn.utils;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @Date 2019/12/12 19:57
 * @Creat by Crane
 */
public class QueryId {

    public static String queryGoodsId(String pId) {
        String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String uid = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 20);
        String gid = pId + date + uid;
        return gid;
    }

    public static String queryEmployeeId(String dId, String phone) {
        String p = phone.substring(phone.length() - 4);
        String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String eId = dId + date + p;
        return eId;
    }
}
