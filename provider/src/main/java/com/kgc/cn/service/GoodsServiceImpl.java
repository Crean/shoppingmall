package com.kgc.cn.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.kgc.cn.mapper.GoodsMapper;
import com.kgc.cn.model.Goods;
import com.kgc.cn.model.GoodsExample;
import com.kgc.cn.utils.QueryId;
import com.kgc.cn.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    /**
     * 得到各类型下商品总数
     *
     * @return
     */
    @Override
    public Map<Page, List<GoodsTypeNumberVo>> queryTypeNumber(int pageSize, int startRow) {
        int totalNum = goodsMapper.queryTypeNumberCount();
        Page page = new Page();
        page.setPage(totalNum, pageSize, startRow);
        GoodsExample goodsExample = new GoodsExample();
        goodsExample.setStartRow(startRow);
        goodsExample.setPageSize(pageSize);
        Map<Page, List<GoodsTypeNumberVo>> pageListMap = Maps.newHashMap();
        pageListMap.put(page, goodsMapper.queryTypeNumber(goodsExample));
        return pageListMap;
    }

    /**
     * 返回商品总销量
     *
     * @return
     */
    @Override
    public Long querySalesVolume() {
        return goodsMapper.querySalesVolume();
    }


    /**
     * 查询展示商品
     *
     * @param goodsQueryVo
     * @return
     */
    @Override
    public Map<Page, List<GoodsQueryVo>> show(GoodsQueryVo goodsQueryVo, int pageSize, int startRow) {
        Goods goods = goodsQueryVo.toGoods();
        GoodsExample goodsExample = new GoodsExample();
        goodsExample.setPageSize(pageSize);
        goodsExample.setStartRow(startRow);
        List<Goods> goodsList = goodsMapper.show(goods, goodsExample);
        List<GoodsQueryVo> goodsQueryVoList = Lists.newArrayList();
        int totalNum = goodsMapper.showCount(goods);
        Page page = new Page();
        page.setPage(totalNum, pageSize, startRow);
        goodsList.forEach(eachGoods -> {
            GoodsQueryVo goodsQueryVos = eachGoods.toGoodsQueryVo();
            goodsQueryVoList.add(goodsQueryVos);
        });
        Map<Page, List<GoodsQueryVo>> pageListMap = Maps.newHashMap();
        pageListMap.put(page, goodsQueryVoList);
        return pageListMap;
    }

    /**
     * 修改商品任意属性
     *
     * @param goodsUpdateVo
     * @return
     */
    @Override
    public boolean updateGoods(GoodsUpdateVo goodsUpdateVo) {
        Goods goods = goodsUpdateVo.toGoods();
        if (goodsMapper.updateByPrimaryKeySelective(goods) > 0) {
            return true;
        }
        return false;
    }

    /**
     * 删除商品
     *
     * @param gid
     * @return
     */
    @Override
    public int deleteGoods(String gid) {
        if (goodsMapper.deleteByPrimaryKey(gid) >= 1) {
            return 1;
        }
        return 0;
    }

    /**
     * 通过excel表格批量添加商品到数据库
     *
     * @param list
     * @return
     */
    @Override
    @Transactional
    public boolean addGoodsToSqlFromExcel(List<GoodsExcelVo> list) {
        list.forEach(goodsExcel -> {
            Goods goods = goodsExcel.toGoods();
            String pid = goodsMapper.selPidByProperty(goods.getgProperty());
            goods.setgId(QueryId.queryGoodsId(pid));
            goodsMapper.insertSelective(goods);
        });
        return true;
    }

    /**
     * 返回总销售额
     *
     * @return
     */
    @Override
    public Long queryTotalSales() {
        return null;
    }

    /**
     * 查询全部商品信息
     *
     * @return
     */
    @Override
    public List<Goods> getTotalGoods() {
        GoodsExample goodsExample = new GoodsExample();
        List<Goods> goodsList = goodsMapper.selectByExample(goodsExample);
        return goodsList;
    }
}
