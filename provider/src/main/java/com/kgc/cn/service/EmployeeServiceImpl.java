package com.kgc.cn.service;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.excel.util.ObjectUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.kgc.cn.mapper.AttendanceMapper;
import com.kgc.cn.mapper.EmployeeMapper;
import com.kgc.cn.model.Attendance;
import com.kgc.cn.model.AttendanceExample;
import com.kgc.cn.model.Employee;
import com.kgc.cn.model.EmployeeExample;
import com.kgc.cn.utils.QueryId;
import com.kgc.cn.utils.activemq.ActiveMqUtils;
import com.kgc.cn.utils.aes.AesUtils;
import com.kgc.cn.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Slf4j
@Service
@SuppressWarnings("all")
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private ActiveMqUtils activeMqUtils;

    @Autowired
    private JavaMailSenderImpl javaMailSender;

    @Autowired
    private AttendanceMapper attendanceMapper;

    /**
     * 冻结用户状态
     *
     * @param id
     * @return
     */
    @Override
    public int updateEmployeeState(String id, Employee employee) {
        if (employee.geteLevel() <= employeeMapper.selectByPrimaryKey(id).geteLevel()) {
            return 2;
        }
        Employee employeeDto = new Employee();
        employeeDto.seteStatus(2);
        employeeDto.seteId(id);
        if (employeeMapper.updateByPrimaryKeySelective(employeeDto) > 0) {
            return 1;
        }
        return 0;
    }

    /**
     * 恢复员工账号
     *
     * @param id
     * @return
     */
    @Override
    public int recoverEmployeeState(String id, Employee employee) {
        if (employee.geteLevel() <= employeeMapper.selectByPrimaryKey(id).geteLevel()) {
            return 2;
        }
        Employee employeeDto = new Employee();
        employeeDto.seteStatus(1);
        employeeDto.seteId(id);
        if (employeeMapper.updateByPrimaryKeySelective(employeeDto) > 0) {
            return 1;
        }
        return 0;
    }

    /**
     * 删除员工
     *
     * @param id
     * @param employee
     * @return
     */
    @Override
    public int delEmployee(String id, Employee employee) {
        if (employee.geteLevel() <= employeeMapper.selectByPrimaryKey(id).geteLevel()) {
            return 2;
        }
        if (employeeMapper.deleteByPrimaryKey(id) > 0) {
            return 1;
        }
        return 0;
    }

    /**
     * 根据任意字段查询员工信息
     *
     * @param employeeQueryVo
     * @return
     */
    @Override
    public Map<Page, List<EmployeeQueryVo>> showEmployee(EmployeeQueryVo employeeQueryVo, int pageSize,
                                                         int startRows, Employee employee) {
        Employee employeeDto = employeeQueryVo.toEmployee();
        EmployeeExample employeeExample = new EmployeeExample();
        employeeExample.setPageSize(pageSize);
        employeeExample.setStartRow(startRows);
        List<Employee> employeeList = employeeMapper.selectEmployee(employeeDto, employeeExample, employee.geteLevel());
        if (CollectionUtils.isNotEmpty(employeeList)) {
            int totalNum = employeeMapper.selectEmployeeCount(employeeDto, employee.geteLevel());
            Page page = new Page();
            page.setPage(totalNum, pageSize, startRows);
            List<EmployeeQueryVo> employeeQueryVoList = Lists.newArrayList();
            employeeList.forEach(eachEmployee -> {
                employeeQueryVoList.add(eachEmployee.toEmployeeQueryVo());
            });
            Map<Page, List<EmployeeQueryVo>> pageListMap = Maps.newHashMap();
            pageListMap.put(page, employeeQueryVoList);
            return pageListMap;
        }
        return null;
    }

    /**
     * 修改员工信息
     *
     * @param employeeUpdateVo
     * @param employee
     * @return
     */
    @Override
    public int updateEmployee(EmployeeUpdateVo employeeUpdateVo, Employee employee) throws Exception {
        Integer level = employeeMapper.selectByPrimaryKey(employeeUpdateVo.geteId()).geteLevel();
        if (employee.geteLevel() < level) {
            return 2;
        }
        if (StringUtils.isNotEmpty(employee.getePassword())) {
            employee.setePassword(AesUtils.encrypt(employee.getePassword()));
        }
        Employee employees = employeeUpdateVo.toEmployee();
        if (employeeMapper.updateByPrimaryKeySelective(employees) > 0) {
            return 1;
        }
        return 0;
    }

    /**
     * 导出员工表为Excel
     *
     * @return
     */
    @Override
    public List<EmployeeExcelVo> showEmployeeExcelVo(int level) {
        List<Employee> employeeList = employeeMapper.showEmployeeExcel()
                .stream().filter(eachEmployee -> (eachEmployee.geteLevel()) <= level).collect(Collectors.toList());
        List<EmployeeExcelVo> employeeExcelVoList = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(employeeList)) {
            employeeList.forEach(eachEmployee -> {
                employeeExcelVoList.add(eachEmployee.toEmployeeExcel());
            });
            return employeeExcelVoList;
        }
        return null;
    }

    /**
     * 员工修改密码
     *
     * @param id
     * @param password
     * @return
     */
    @Override
    public int updatePassword(String id, String password) throws Exception {
        String aesPassword = AesUtils.encrypt(password);
        if (employeeMapper.updatePassword(aesPassword, id) > 0) {
            return 1;
        }
        return 0;
    }

    /**
     * 根据id查询
     *
     * @param eid
     * @return
     */
    @Override
    public boolean queryEmployeeById(String eid) {
        Employee employee = employeeMapper.selectByPrimaryKey(eid);
        if (ObjectUtils.isEmpty(employee)) {
            return true;
        }
        return false;
    }

    /**
     * 请假开始
     *
     * @param eid
     */
    @Override
    public void vacationEmployee(String eid) {
        Employee employee = new Employee();
        employee.seteId(eid);
        employee.seteAttendance(3);
        employeeMapper.updateByPrimaryKeySelective(employee);
    }

    /**
     * 请假结束
     *
     * @param eid
     */
    @Override
    public void vacationExpire(String eid) {
        Employee employee = new Employee();
        employee.seteId(eid);
        employee.seteAttendance(0);
        employeeMapper.updateByPrimaryKeySelective(employee);
    }

    /**
     * 员工登录
     *
     * @param employeeLoginVo
     * @return
     */
    @Override
    public Employee employeeLogin(EmployeeLoginVo employeeLoginVo) throws Exception {
        // 根据工号查员工
        Employee employee = employeeMapper.selectByPrimaryKey(employeeLoginVo.geteId());
        if (!ObjectUtils.isEmpty(employee)) {
            // 验证密码
            if (AesUtils.decrypt(employee.getePassword()).equals(employeeLoginVo.getePassword())) {
                if (employee.geteAttendance() == 0) {
                    int date = Integer.parseInt(new SimpleDateFormat("HHmm").format(new Date()));
                    if (date <= 900) {
                        employee.seteAttendance(1);
                    } else if (date > 900 && date <= 1000) {
                        employee.seteAttendance(2);
                    }
                    employeeMapper.updateByPrimaryKeySelective(employee);
                }
                return employee;
            }
        }
        return null;
    }

    /**
     * 通过excel批量增加员工信息
     *
     * @param list
     * @return
     */
    @Override
    @Transactional
    public boolean addEmpFromExcel(List<EmployeeExcelVo> list) {
        List<Employee> employeeList = Lists.newArrayList();
        list.forEach(employeeExcel -> {
            //避开重复员工信息
            if (employeeMapper.selPhone(employeeExcel.getPhone()) == 0) {
                Employee employee = employeeExcel.toEmployee();
                //生成工号
                String dId = employeeMapper.selEidFromDepartment(employee.geteDepartment());
                try {
                    employee.setePassword(AesUtils.encrypt("000000"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                employee.seteId(QueryId.queryEmployeeId(dId, employee.getePhone()));
                //存入数据库
                employeeList.add(employee);
                employeeMapper.insertSelective(employee);
            }
        });
        employeeList.forEach(eachEmployee -> {
            activeMqUtils.sendMsgByQueue("email", eachEmployee);
        });
        return true;
    }


    @JmsListener(destination = "email")
    public void listenerAndSendEmail(Employee eachEmployee) {
        sendMail(eachEmployee);
    }


    //发送邮件方法
    @Async
    public void sendMail(Employee eachEmployee) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setFrom("13889561476@163.com");
        msg.setTo(eachEmployee.geteEmail());
        msg.setSubject("入职通知");
        msg.setText("您的账号已激活\n您的工号为：" + eachEmployee.geteId() + "您的初始密码为：000000");
        javaMailSender.send(msg);
    }

    /**
     * excel输出一个月内某天的出勤情况
     *
     * @param date
     * @return
     * @throws ParseException
     */
    @Override
    public List<AttendanceExcelVo> showAttendToExcel(String date) {
        String[] dateStr = date.split(",");
        List<Date> dateList = Lists.newArrayList();
        Arrays.asList(dateStr).stream().forEach(dates -> {
            try {
                dateList.add(new SimpleDateFormat("yyyy/MM/dd").parse(dates));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
        AttendanceExample example = new AttendanceExample();
        example.createCriteria().andADateIn(dateList);
        List<Attendance> attendanceList = attendanceMapper.selectByExample(example);
        List<AttendanceExcelVo> attendanceExcelVo = Lists.newArrayList();
        attendanceList.forEach(attExcel -> {
            attendanceExcelVo.add(attExcel.toAttendanceExcelVo());
        });
        return attendanceExcelVo;
    }


}
