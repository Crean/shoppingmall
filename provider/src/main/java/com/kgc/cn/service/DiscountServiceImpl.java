package com.kgc.cn.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.enums.Enums;
import com.kgc.cn.exception.GoodsDiscountException;
import com.kgc.cn.mapper.DiscountMapper;
import com.kgc.cn.mapper.GoodsMapper;
import com.kgc.cn.model.Discount;
import com.kgc.cn.vo.DiscountVo;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

@Log4j
@Service
public class DiscountServiceImpl implements DiscountService {

    @Autowired
    private DiscountMapper discountMapper;
    @Autowired
    private GoodsMapper goodsMapper;

    /**
     * 新增商品折扣信息
     *
     * @param discountVoList
     * @return
     */
    @Override
    @Transactional
    public int addGoodsDiscount(List<DiscountVo> discountVoList) {
        discountVoList.forEach(discountVo -> {
            Discount discount = discountVo.toDiscount();
            if (goodsMapper.selectGoodsDiscount(discountVo.getId()) != 0) {
                throw new GoodsDiscountException(Enums.GoodsEnum.ADD_DISCOUNT_FAIL);
            }
            discountMapper.insertDiscount(discount);
            goodsMapper.insertGoodsAndDiscount(discount.getdId().toString(), discountVo.getId());
        });
        return 1;
    }

    /**
     * 修改商品折扣
     *
     * @param discountVo
     * @return
     */
    @Override
    public int updateGoodsDiscount(DiscountVo discountVo) {
        String dId = goodsMapper.selecDiscountByGid(discountVo.getId());
        if (StringUtils.isEmpty(dId)) {
            return 2;
        }
        Discount discount = discountVo.toDiscount();
        discount.setdId(Long.valueOf(dId));
        if (discountMapper.updateByPrimaryKey(discount) > 0) {
            return 1;
        }
        return 0;
    }
}
