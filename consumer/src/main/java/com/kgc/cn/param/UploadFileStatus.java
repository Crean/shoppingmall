package com.kgc.cn.param;

import lombok.Data;

import java.io.FileInputStream;

@Data
public class UploadFileStatus {


    private String fileName;
    private String fileType;
    private String filePath;
    private FileInputStream fileInputStream;
    private long fileSize;

}
