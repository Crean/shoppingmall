package com.kgc.cn.listener;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.service.EmployeeService;
import com.kgc.cn.utils.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

public class KeyExpiredListener extends KeyExpirationEventMessageListener {
    @Reference
    private EmployeeService employeeService;
    @Autowired
    private RedisUtils redisUtils;

    public KeyExpiredListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        String[] key = message.toString().split(",");
        //todo 这里能拿到的key就是过期的key，既然我知道了这个key过期了，流水
        if (key.length == 4 && "vacationStart".equals(key[3])) {
            employeeService.vacationEmployee(key[0]);
            int expireTime = Integer.parseInt(key[2]) * 86400;
            redisUtils.set(key[0] + "," + key[1] + ",vacationExpires", 2, expireTime);
        }
        if (key.length == 3 && "vacationExpires".equals(key[2])) {
            employeeService.vacationExpire(key[0]);
        }
    }
}
