package com.kgc.cn.config.aop.impl;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.config.aop.Level3Required;
import com.kgc.cn.model.Employee;
import com.kgc.cn.utils.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class Level3Checked implements HandlerInterceptor {

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();

        Level3Required level3Required = method.getAnnotation(Level3Required.class);
        if (level3Required != null) {
            String token = request.getHeader("staff_token");
            if (StringUtils.isNotEmpty(token)) {
                String userJsonStr = (String) redisUtils.get(token);
                if (StringUtils.isNotEmpty(userJsonStr)) {
                    Employee employee = JSONObject.parseObject(userJsonStr, Employee.class);
                    if (employee.geteLevel() >= 3) {
                        request.setAttribute("userJsonString", userJsonStr);
                    } else {
                        throw new Exception("Need authority");
                    }
                } else {
                    throw new Exception("Need authority");
                }
            } else {
                throw new Exception("Need authority");
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
