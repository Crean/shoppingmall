package com.kgc.cn.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.config.aop.Level2Required;
import com.kgc.cn.config.aop.Level3Required;
import com.kgc.cn.config.aop.StaffLoginRequired;
import com.kgc.cn.enums.Enums;
import com.kgc.cn.model.Goods;
import com.kgc.cn.service.GoodsService;
import com.kgc.cn.utils.exceI.ExcelUtils;
import com.kgc.cn.utils.returns.ReturnResult;
import com.kgc.cn.utils.returns.ReturnResultUtils;
import com.kgc.cn.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

@Api(tags = "商品")
@RestController
@RequestMapping(value = "/goods")
public class GoodsController {

    @Reference
    private GoodsService goodsService;

    /**
     * 商品查询展示
     */
    @StaffLoginRequired
    @ApiOperation(value = "商品展示")
    @PostMapping(value = "/show")
    public ReturnResult<Map<Page, List<GoodsQueryVo>>> show(@Valid GoodsQueryVo goodsQueryVo,
                                                            @ApiParam("页面条数") @RequestParam(defaultValue = "10") int pageSize,
                                                            @ApiParam("起始页数") @RequestParam(defaultValue = "1") int startRow) {
        Map<Page, List<GoodsQueryVo>> goodsQueryVoMap = goodsService.show(goodsQueryVo, pageSize, startRow);
        if (!CollectionUtils.isEmpty(goodsQueryVoMap)) {
            return ReturnResultUtils.returnSuccess(goodsQueryVoMap);
        }
        return ReturnResultUtils.returnFail(Enums.GoodsEnum.SELECT_FAIL);
    }

    /**
     * 修改商品
     */
    @Level3Required
    @ApiOperation(value = "商品修改")
    @PostMapping(value = "/updateGoods")
    public ReturnResult updateGoods(@Valid GoodsUpdateVo goodsUpdateVo) {
        if (goodsService.updateGoods(goodsUpdateVo)) {
            return ReturnResultUtils.returnSuccess(Enums.GoodsEnum.UPDATE_SUCCESS);
        }
        return ReturnResultUtils.returnFail(Enums.GoodsEnum.UPDATE_FAIL);
    }

    /**
     * 删除商品
     */
    @Level3Required
    @ApiOperation(value = "商品删除")
    @PostMapping(value = "/deleteGoods")
    public ReturnResult<Goods> deleteGoods(@ApiParam(value = "商品id") @RequestParam String gid) {
        if (goodsService.deleteGoods(gid) == 1) {
            return ReturnResultUtils.returnSuccess(Enums.GoodsEnum.DELETE_SUCCESS);
        }
        return ReturnResultUtils.returnFail(Enums.GoodsEnum.DELETE_FAIL);
    }

    @StaffLoginRequired
    @ApiOperation(value = "商品类型个数")
    @PostMapping(value = "/queryTypeNumber")
    public ReturnResult<Map<Page, List<GoodsTypeNumberVo>>> queryTypeNumber(@ApiParam("页面条数") @RequestParam(defaultValue = "10") int pageSize,
                                                                            @ApiParam("起始页数") @RequestParam(defaultValue = "1") int startRow) {
        return ReturnResultUtils.returnSuccess(goodsService.queryTypeNumber(pageSize, startRow));
    }

    @StaffLoginRequired
    @ApiOperation(value = "商品总销售量")
    @PostMapping(value = "/querySalesVolume")
    public ReturnResult<Long> querySalesVolume() {
        return ReturnResultUtils.returnSuccess(goodsService.querySalesVolume());
    }

    @StaffLoginRequired
    @ApiOperation(value = "总销售额")
    @PostMapping(value = "/totalSales")
    public ReturnResult<Long> queryTotalSales() {
        return ReturnResultUtils.returnSuccess(goodsService.queryTotalSales());
    }


    @Level2Required
    @ApiOperation("通过excel批量增加商品")
    @PostMapping(value = "/addGoods")
    public ReturnResult addGoodsToSqlFromExcel(MultipartFile file) throws Exception {
        // 判断文件是否为空
        if (file.isEmpty()) {
            return ReturnResultUtils.returnFail(Enums.FileEnum.FILE_NOT_EXIST);
        }
        // 将文件转化成流
        InputStream in = file.getInputStream();
        // 将流转化成对应的excel表格的实体类
        Map map = ExcelUtils.readExcelByModelFromInputStream(in, "goods", GoodsExcelVo.class);
        // 将List<Object>转换为List<GoodsExcelVo>
        List<GoodsExcelVo> list = (List) map.get("goods");
        // 将实体类集合入库
        if (goodsService.addGoodsToSqlFromExcel(list))
            return ReturnResultUtils.returnSuccess(Enums.GoodsEnum.ADD_GOODS_SUCCESS);
        return ReturnResultUtils.returnFail(Enums.GoodsEnum.ADD_GOODS_FAIL);
    }

}
