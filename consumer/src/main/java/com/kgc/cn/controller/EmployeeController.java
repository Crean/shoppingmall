package com.kgc.cn.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.config.aop.CurrentEmployee;
import com.kgc.cn.config.aop.Level3Required;
import com.kgc.cn.config.aop.Level4Required;
import com.kgc.cn.config.aop.StaffLoginRequired;
import com.kgc.cn.enums.Enums;
import com.kgc.cn.model.Employee;
import com.kgc.cn.service.EmployeeService;
import com.kgc.cn.utils.date.DateUtils;
import com.kgc.cn.utils.exceI.ExcelUtils;
import com.kgc.cn.utils.redis.RedisUtils;
import com.kgc.cn.utils.returns.ReturnResult;
import com.kgc.cn.utils.returns.ReturnResultUtils;
import com.kgc.cn.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Api(tags = "员工")
@RestController
@RequestMapping(value = "/emp")
public class EmployeeController {

    @Reference
    private EmployeeService employeeService;
    @Autowired
    private RedisUtils redisUtils;

    @PostMapping(value = "/login")
    @ApiOperation(value = "员工登录")
    public ReturnResult employeeLogin(@Valid EmployeeLoginVo employeeLoginVo, HttpServletRequest request) throws Exception {
        Employee employee = employeeService.employeeLogin(employeeLoginVo);
        if (!ObjectUtils.isEmpty(employee)) {
            if (employee.geteStatus() == 2) {
                return ReturnResultUtils.returnFail(Enums.EmployeeEnum.EMPLOYEE_IS_FREEZE);
            }
            String staff_token = request.getSession().getId();
            String employeeJsonStr = JSONObject.toJSONString(employee);
            redisUtils.set(staff_token, employeeJsonStr, 30 * 60);
            redisUtils.set(Enums.CommonEnum.TOKEN_NAME_SPACE + employee.geteId(), staff_token, 30 * 60);
            return ReturnResultUtils.returnSuccess(staff_token);
        } else {
            return ReturnResultUtils.returnFail(Enums.CommonEnum.LOGIN_ERROR);
        }
    }

    @StaffLoginRequired
    @GetMapping(value = "/out")
    @ApiOperation(value = "注销")
    public ReturnResult<String> employeeOut(HttpServletRequest request) {
        String token = request.getHeader("staff_token");
        redisUtils.del(token);
        return ReturnResultUtils.returnSuccess(Enums.CommonEnum.OUT_SUCCESS);
    }

    @Level3Required
    @ApiOperation(value = "通过excel表格批量添加员工并发送邮件")
    @PostMapping(value = "/addEmployee")
    public ReturnResult addEmpFromExcel(@ApiParam("员工信息表格") MultipartFile file) throws Exception {
        // 判断文件是否为空
        if (file.isEmpty()) return ReturnResultUtils.returnFail(Enums.FileEnum.FILE_NOT_EXIST);
        // 将文件转化成流
        InputStream in = file.getInputStream();
        // 将流转化成对应的excel表格的实体类
        Map map = ExcelUtils.readExcelByModelFromInputStream(in, "employee", EmployeeExcelVo.class);
        // 将List<Object>转换为List<EmployeeExcelVo>
        List<EmployeeExcelVo> list = (List) map.get("employee");
        // 将实体类集合入库
        if (employeeService.addEmpFromExcel(list))
            return ReturnResultUtils.returnSuccess(Enums.EmployeeEnum.EMPLOYEE_ADD_SUCCESS);
        return ReturnResultUtils.returnFail(Enums.EmployeeEnum.EMPLOYEE_ADD_FAIL);
    }

    @ApiOperation(value = "以excel表格形式下载某天员工出勤情况")
    @GetMapping(value = "/showAttend")
    public void showAttend(HttpServletResponse response,
                           @ApiParam("需要查询的日期(yyyy/MM/dd)") @RequestParam String date) throws Exception {
        ServletOutputStream out = response.getOutputStream();
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
        String fileName = new String("attendance".getBytes(), "utf-8");
        Sheet sheet = new Sheet(1, 0, AttendanceExcelVo.class);
        sheet.setSheetName("sheet");
        List<AttendanceExcelVo> list = employeeService.showAttendToExcel(date);
        response.setContentType("application/octet-stream; charset=utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        writer.write(list, sheet);
        writer.finish();
        out.flush();
    }

    @StaffLoginRequired
    @ApiOperation("员工查看个人信息")
    @GetMapping(value = "/showEmployee")
    public ReturnResult<EmployeeQueryVo> showEmployee(@CurrentEmployee Employee employee) {
        EmployeeQueryVo employeeQueryVo = employee.toEmployeeQueryVo();
        return ReturnResultUtils.returnSuccess(employeeQueryVo);
    }

    @Level3Required
    @ApiOperation("冻结员工账号")
    @GetMapping(value = "/updateEmployeeState")
    public ReturnResult<String> updateEmployeeState(@ApiParam("员工工号") @RequestParam String id,
                                                    @CurrentEmployee Employee employee) {
        int flag = employeeService.updateEmployeeState(id, employee);
        if (flag == 1) {
            redisUtils.del(Enums.CommonEnum.TOKEN_NAME_SPACE + id);
            return ReturnResultUtils.returnSuccess(Enums.EmployeeEnum.FREEZE_SUCCESS);
        } else if (flag == 2) {
            return ReturnResultUtils.returnFail(Enums.EmployeeEnum.UPDATE_LEVEL_FAIL);
        }
        return ReturnResultUtils.returnFail(Enums.EmployeeEnum.FREEZE_ERROR);
    }

    @Level3Required
    @ApiOperation(value = "恢复员工账号")
    @GetMapping(value = "/recoverEmployeeState")
    public ReturnResult<String> recoverEmployeeState(@ApiParam("员工工号") @RequestParam String id,
                                                     @CurrentEmployee Employee employee) {
        int flag = employeeService.recoverEmployeeState(id, employee);
        if (flag == 1) {
            return ReturnResultUtils.returnSuccess(Enums.EmployeeEnum.RECORD_SUCCESS);
        } else if (flag == 2) {
            return ReturnResultUtils.returnFail(Enums.EmployeeEnum.UPDATE_LEVEL_FAIL);
        }
        return ReturnResultUtils.returnFail(Enums.EmployeeEnum.RECORD_ERROR);
    }

    @Level4Required
    @ApiOperation("删除员工账号")
    @GetMapping(value = "/delEmployee")
    public ReturnResult<String> delEmployee(@ApiParam("员工工号") @RequestParam String id,
                                            @CurrentEmployee Employee employee) {
        int flag = employeeService.delEmployee(id, employee);
        if (flag == 1) {
            redisUtils.del(Enums.CommonEnum.TOKEN_NAME_SPACE + id);
            return ReturnResultUtils.returnSuccess(Enums.EmployeeEnum.DEL_SUCCESS);
        } else if (flag == 2) {
            return ReturnResultUtils.returnFail(Enums.EmployeeEnum.UPDATE_LEVEL_FAIL);
        }
        return ReturnResultUtils.returnFail(Enums.EmployeeEnum.DEL_ERROR);
    }


    @Level3Required
    @ApiOperation("管理员查看员工信息")
    @PostMapping(value = "/showEmployee")
    public ReturnResult<Map<Page, List<EmployeeQueryVo>>> showEmployee(@Valid EmployeeQueryVo employeeQueryVo,
                                                                       @ApiParam("页面条数") @RequestParam(defaultValue = "10") int pageSize,
                                                                       @ApiParam("起始页数") @RequestParam(defaultValue = "1") int startRows,
                                                                       @CurrentEmployee Employee employee) {
        Map<Page, List<EmployeeQueryVo>> employeeQueryVoMap = employeeService.showEmployee(employeeQueryVo, pageSize, startRows, employee);
        if (!CollectionUtils.isEmpty(employeeQueryVoMap)) {
            return ReturnResultUtils.returnSuccess(employeeQueryVoMap);
        }
        return ReturnResultUtils.returnFail(Enums.EmployeeEnum.QUERY_ERROR);
    }

    @Level3Required
    @ApiOperation("修改员工信息")
    @PostMapping(value = "/updateEmployee")
    public ReturnResult<String> updateEmployee(@Valid EmployeeUpdateVo employeeUpdateVo,
                                               @CurrentEmployee Employee employee) throws Exception {
        int flag = employeeService.updateEmployee(employeeUpdateVo, employee);
        if (flag == 1) {
            return ReturnResultUtils.returnSuccess(Enums.EmployeeEnum.UPDATE_SUCCESS);
        } else if (flag == 2) {
            return ReturnResultUtils.returnFail(Enums.EmployeeEnum.UPDATE_LEVEL_FAIL);
        }
        return ReturnResultUtils.returnFail(Enums.EmployeeEnum.UPDATE_ERROR);
    }


    @Level3Required
    @ApiOperation("以excel表格形式下载员工表")
    @GetMapping(value = "/employeeExcelFile")
    public void empolyeeExcelFile(HttpServletResponse response,
                                  @CurrentEmployee Employee employee) throws Exception {
        ServletOutputStream out = response.getOutputStream();
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
        String fileName = new String(("employee" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()))
                .getBytes(), "UTF-8");
        Sheet sheet = new Sheet(1, 0, EmployeeExcelVo.class);
        sheet.setSheetName("sheet");
        List<EmployeeExcelVo> list = employeeService.showEmployeeExcelVo(employee.geteLevel());
        response.setContentType("application/octet-stream; charset=utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        writer.write(list, sheet);
        writer.finish();
        out.flush();
    }


    @StaffLoginRequired
    @ApiOperation("员工修改密码")
    @GetMapping(value = "/updatePassword")
    public ReturnResult<String> updatePassword(@ApiParam("新密码") @RequestParam String password,
                                               @CurrentEmployee Employee employee, HttpServletRequest request) throws Exception {
        if (employeeService.updatePassword(employee.geteId(), password) == 1) {
            employeeOut(request);
            return ReturnResultUtils.returnSuccess(Enums.EmployeeEnum.UPDATEPASSWORD_SUCCESS);
        }
        return ReturnResultUtils.returnFail(Enums.EmployeeEnum.UPDATEPASSWORD_FAIL);
    }

    @Level3Required
    @ApiOperation(value = "批准员工请假")
    @PostMapping(value = "/vacationEmployee")
    public ReturnResult<String> vacationEmployee(@ApiParam(value = "员工id", required = true) @RequestParam String eid,
                                                 @ApiParam(value = "请假开始日期，格式为yyyy-MM-dd", required = true) @RequestParam String date,
                                                 @ApiParam(value = "请假天数", required = true) @RequestParam String day) throws ParseException {
        if (!DateUtils.isRqFormat(date)) {
            return ReturnResultUtils.returnFail(Enums.CommonEnum.DATE_ERROR);
        }
        if (Integer.parseInt(day) <= 0) {
            return ReturnResultUtils.returnFail(Enums.CommonEnum.DAY_ERROR);
        }
        if (employeeService.queryEmployeeById(eid)) {
            return ReturnResultUtils.returnFail(Enums.CommonEnum.EID_NOT_EXIST);
        }
        long nowTime = System.currentTimeMillis();
        long startTime = new SimpleDateFormat("yyyy-MM-dd").parse(date).getTime();
        int expireTime = (int) (startTime - nowTime) / 1000;
        redisUtils.set(eid + "," + date + "," + day + "," + "vacationStart", 1, expireTime);

        return ReturnResultUtils.returnSuccess(Enums.CommonEnum.VACATION_SUCCESS);
    }
}
